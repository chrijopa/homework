cmake_minimum_required (VERSION 2.6)

project (Homework)

set(SOURCE_FILES
	src/Hexahedron.cpp
	src/main.cpp
	src/Tetrahedron.cpp
	src/VolumeTopology.cpp
)

add_executable(${PROJECT_NAME} ${SOURCE_FILES})