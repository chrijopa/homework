// make sure that the class can not be included twice
# ifndef Tetrahedron_H
# define Tetrahedron_H

#include "VolumeTopology.h"

class Tetrahedron : public VolumeTopology
{
public:
	// constructors
	Tetrahedron();
	Tetrahedron(std::vector<unsigned int> connect);
	// destructors
	virtual ~Tetrahedron();

	// function to get the edge point ids
	std::vector< std::vector < unsigned int > > getEdgePtIds();
};

# endif