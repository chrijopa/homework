// make sure that the class can not be included twice
# ifndef Hexahedron_H
# define Hexahedron_H

#include "VolumeTopology.h"

class Hexahedron : public VolumeTopology
{
public:
	// constructors
	Hexahedron();
	Hexahedron(std::vector<unsigned int> connect);
	// destructors
	virtual ~Hexahedron();

	// function to get the edge point ids
	std::vector< std::vector < unsigned int > > getEdgePtIds();
};

# endif