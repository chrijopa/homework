#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <typeinfo>
#include <cxxabi.h>
#include <ctime>

// Allow usage of the function name with the different compiler types
#ifndef __func__
#define __func__ __FUNCTION__
#endif

#ifndef NDEBUG	// "not not debug" = debug
	// Output of a method call
	#define LOG_FUNCALL \
	char*className=abi::__cxa_demangle(typeid( *this ).name(),0,0,NULL); \
	std::cout << className << "::" << __func__ << " called" << std::endl

	// Output of the current location in the code
	#define DEBUG std::cerr << __FILE__ << ": " << __LINE__ <<  std::endl
	#define DEBUGB(a) if(a) DEBUG

	// Show variables and todos
	#define SHOW(a) DEBUG << " : " << #a << " : " << (a) << std::endl
	#define TODO(a) DEBUG << " : TODO : " << (a) << std::endl
	  
	// Show the advancement of the code
	#define NOT_IMPLEMENTED_METHOD           \
	  char*className=abi::__cxa_demangle(typeid( *this ).name(),0,0,NULL); \
	  DEBUG << " : " \
	  << className << "::" << __func__ << " : not implemented !" << std::endl                            
	#define NOT_IMPLEMENTED_FUNCTION         \
	  DEBUG << " : " << __func__ << " : not implemented !" << std::endl
	  
	// A measurement of time
	#define TIMER_START(x) clock_t ___time__##x = clock() ; 
	#define TIMER_END(x) \
	  double ___timer__##x = double(clock() - ___timer__##x)/CLOCKS_PER_SEC;\
	  std::cout << "t" << #x << ": " << ___timer__##x << endl;
#else
	#define LOG_FUNCALL
	#define DEBUG
	#define DEBUGB(a)
	#define SHOW(a)
	#define TODO(a)
	#define NOT_IMPLEMENTED_METHOD        
	#define NOT_IMPLEMENTED_FUNCTION
	#define TIMER_START(x)
	#define TIMER_END(x)
#endif

#endif // UTILS_H
