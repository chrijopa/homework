// make sure that the class can not be included twice
# ifndef VolumeTopology_H
# define VolumeTopology_H
// preprocessor line to include the vector class
#include <vector>

class VolumeTopology
{
public:
	// constructors
	VolumeTopology();
	VolumeTopology(std::vector<unsigned int> connect);
	// destructors
	virtual ~VolumeTopology();

	// methods to get internal values
	std::vector<unsigned int> getConnect();
private:
	// data member containing the connectivity of our grid
	std::vector<unsigned int> connect;
};

# endif