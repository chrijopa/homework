#include "../include/VolumeTopology.h"

// empty constructor
VolumeTopology::VolumeTopology()
{

}

// constructor defining our data
VolumeTopology::VolumeTopology(std::vector<unsigned int> connect)
{
	this->connect = connect;
}

// destructors
VolumeTopology::~VolumeTopology()
{

}

// methods to get internal values
std::vector<unsigned int> VolumeTopology::getConnect()
{
	return connect;
}