#include <iostream>
#include <vector>
#include "../include/VolumeTopology.h"
#include "../include/Tetrahedron.h"
#include "../include/Hexahedron.h"

int main (int argc, char *argv[])
{
	// output of the given parameters
	for( int i = 0; i < argc; i++ )
	{
		std::cout << "Argument " << i
				  << " has the value " << argv[i]
				  << std::endl;
	}

	// define the vector of connected points
	std::vector<unsigned int> connect;
	connect.push_back(0);
	connect.push_back(1);
	connect.push_back(3);
	connect.push_back(2);

	// initiate an instance of our volumetopology
	Tetrahedron tet(connect);

	// output the connect of our topology
	std::vector<unsigned int> otherConnect = tet.getConnect();
	for (unsigned int i=0; i<otherConnect.size(); i++)
		std::cout << otherConnect[i] << " ";

	// output the edge point ids
	std::cout << std::endl;
	std::vector< std::vector < unsigned int > > edges = tet.getEdgePtIds();
	for (unsigned int i=0; i<edges.size(); i++)
	{
		std::cout << edges[i][0] << " ";
		std::cout << edges[i][1] << std::endl;
	}

	// declaration of the points
	std::vector< std::vector <double> > points;
	// define the size of the resulting vector:
	// we have four points...
	points.resize(4);
	for (unsigned int i=0; i<points.size(); i++)
	// ... and every point has three entries
	points[i].resize(3);
	// definition of the values for the nodes
	// point 0
	(points[0])[0] = 0.0d;
	(points[0])[1] = 0.0d;
	(points[0])[2] = 0.0d;
	// point 1
	(points[1])[0] = 1.0d;
	(points[1])[1] = 0.0d;
	(points[1])[2] = 0.0d;
	// point 2
	(points[2])[0] = 0.0d;
	(points[2])[1] = 1.0d;
	(points[2])[2] = 0.0d;
	// point 3
	(points[3])[0] = 0.0d;
	(points[3])[1] = 0.0d;
	(points[3])[2] = 1.0d;

	// output the middle points of the edges
	for (unsigned int i=0; i<edges.size(); i++)
	{
		for(unsigned int j=0; j<3; j++)
		{
			std::cout << 0.5*(points[edges[i][0]][j] + points[edges[i][1]][j]) << " ";
		}
		std::cout << std::endl;	
	}
}
