#include "../include/Tetrahedron.h"

Tetrahedron::Tetrahedron() :
VolumeTopology()
{
}

Tetrahedron::Tetrahedron(std::vector<unsigned int> connect) :
VolumeTopology(connect)
{
}

Tetrahedron::~Tetrahedron()
{
}

std::vector< std::vector < unsigned int > > Tetrahedron::getEdgePtIds()
{
	// get the connect
	std::vector<unsigned int> connect = this->getConnect();
	// declare the result
	std::vector< std::vector < unsigned int > > edges;
	// define the size of the resulting vector:
	// we have six edges ...
	edges.resize(6);
	for (unsigned int i=0; i<edges.size(); i++)
	// ... and every edge has two point ids
	edges[i].resize(2);
	// define the edges
	// define edge number 0
	(edges[0])[0] = connect[0]; // first point
	(edges[0])[1] = connect[1]; // second point
	// define edge number 1
	(edges[1])[0] = connect[0];
	(edges[1])[1] = connect[2];
	// define edge number 2
	(edges[2])[0] = connect[0];
	(edges[2])[1] = connect[3];
	// define edge number 3
	(edges[3])[0] = connect[1];
	(edges[3])[1] = connect[2];
	// define edge number 4
	(edges[4])[0] = connect[1];
	(edges[4])[1] = connect[3];
	// define edge number 5
	(edges[5])[0] = connect[2];
	(edges[5])[1] = connect[3];
	// return the result
	return edges;
}